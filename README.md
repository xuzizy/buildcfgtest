计算length-n subpath的测试用例。输出length-n subpath的覆盖率。
记录一次运行经过的length-n subpath的数量，再除以总length-n subpath路径数。
输入文件：length_n_subpath.count
输出文件：length_n_subpath.coverage(包括所有length-n subpath，以及覆盖路径)

PS: You can find my buildcfg llvm pass code in https://bitbucket.org/xuzizy/buildcfg
