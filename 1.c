#include <stdlib.h>
#include <stdio.h>
int f(int);
void g(int,int);

int main(int argc ,char* argv[]){
	if(argc != 3) {
		fprintf(stderr,"usage: ./program int1 int2\n");
		exit(-1);
	}
	int x = atoi(argv[1]);
	int y = atoi(argv[2]);  
	if(x > y)
		x = f(x);
	else
		y++;


	printf("x = %d",x);
	switch(x){
		case 0: g(x,y); break; 
		default: return -1;
	}
	return 0;
}

int f(int a){
	if( a > 0)
		exit(0);
	else 
		return -a;
}

void g(int a,int b){
	if(a == 0)
		if(b == 0)
			exit(0);//unreachable
		else{
			while(a < 10)
				a++;
		}
	else
		printf("a/b = %d\n",a/b );//unreachable

	return ;
}