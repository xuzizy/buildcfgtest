BUILDCFG=~/work/llvm-2.9/Release+Asserts/lib
MYPASS=~/work/llvm-2.9/lib/Transforms/buildCFG
CFLAGS=   -Werror -emit-llvm -g
NAME =
SUBPATHLENGTH = 4

all:$(NAME).ll $(NAME)buildCFG.ll $(NAME)_final.bc 

$(NAME).ll:$(NAME).bc
	llvm-dis $(NAME).bc
$(NAME)buildCFG.ll:$(NAME)buildCFG.bc
	llvm-dis $(NAME)buildCFG.bc

$(NAME)_final.bc:$(NAME)buildCFG.bc $(MYPASS)/mylib/ExecutePath.bc
	llvm-ld $(NAME)buildCFG.bc $(MYPASS)/mylib/ExecutePath.bc -o $(NAME)_final

$(MYPASS)/mylib/ExecutePath.bc:
	make -C $(MYPASS)/mylib/
 

$(NAME)buildCFG.bc:$(NAME).bc $(BUILDCFG)/buildCFG.so
	opt -dot-cfg-only -load $(BUILDCFG)/buildCFG.so -buildCFG -n=$(SUBPATHLENGTH) <$(NAME).bc >$(NAME)buildCFG.bc

$(BUILDCFG)/buildCFG.so:
	make -C $(MYPASS)/ 

$(NAME).bc:$(NAME).c
	llvm-gcc -c $(CFLAGS) $(NAME).c -o $(NAME).bc

clean:
	rm -f *.bc
	make -C $(MYPASS)/ clean
	make -C $(MYPASS)/mylib clean
