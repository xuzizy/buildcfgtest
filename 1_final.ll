; ModuleID = '1_final.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

%0 = type { i32, void ()* }
%struct..1__pthread_mutex_s = type { i32, i32, i32, i32, i32, %union..0._10 }
%struct.__pthread_slist_t = type { %struct.__pthread_slist_t* }
%"struct.std::basic_ios<char,std::char_traits<char> >" = type { %"struct.std::ios_base", %"struct.std::basic_ostream<char,std::char_traits<char> >"*, i8, i8, %"struct.std::basic_streambuf<char,std::char_traits<char> >"*, %"struct.std::ctype<char>"*, %"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >"*, %"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >"* }
%"struct.std::basic_ostream<char,std::char_traits<char> >" = type { i32 (...)**, %"struct.std::basic_ios<char,std::char_traits<char> >" }
%"struct.std::basic_streambuf<char,std::char_traits<char> >" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"struct.std::locale" }
%"struct.std::ctype<char>" = type { %"struct.std::locale::facet", i32*, i8, i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8 }
%"struct.std::ios_base" = type { i32 (...)**, i32, i32, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"struct.std::locale" }
%"struct.std::ios_base::Init" = type <{ i8 }>
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"struct.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i32 }
%"struct.std::locale" = type { %"struct.std::locale::_Impl"* }
%"struct.std::locale::_Impl" = type { i32, %"struct.std::locale::facet"**, i32, %"struct.std::locale::facet"**, i8** }
%"struct.std::locale::facet" = type { i32 (...)**, i32 }
%"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >" = type { %"struct.std::locale::facet" }
%"struct.std::num_put<char,std::ostreambuf_iterator<char, std::char_traits<char> > >" = type { %"struct.std::locale::facet" }
%union..0._10 = type { i32 }
%union.pthread_attr_t = type { i32, [8 x i32] }
%union.pthread_mutex_t = type { %struct..1__pthread_mutex_s }
%union.pthread_mutexattr_t = type { i32 }

@.str = private constant [10 x i8] c"a/b = %d\0A\00", align 1
@0 = internal constant [11 x i8] c"main.entry\00"
@1 = internal constant [8 x i8] c"main.bb\00"
@2 = internal constant [9 x i8] c"main.bb1\00"
@3 = internal constant [12 x i8] c"main.return\00"
@4 = internal constant [8 x i8] c"f.entry\00"
@5 = internal constant [5 x i8] c"f.bb\00"
@6 = internal constant [6 x i8] c"f.bb1\00"
@7 = internal constant [9 x i8] c"f.return\00"
@8 = internal constant [8 x i8] c"g.entry\00"
@9 = internal constant [5 x i8] c"g.bb\00"
@10 = internal constant [6 x i8] c"g.bb1\00"
@11 = internal constant [6 x i8] c"g.bb2\00"
@12 = internal constant [6 x i8] c"g.bb3\00"
@13 = internal constant [6 x i8] c"g.bb4\00"
@14 = internal constant [6 x i8] c"g.bb5\00"
@15 = internal constant [6 x i8] c"g.bb6\00"
@16 = internal constant [6 x i8] c"g.bb7\00"
@17 = internal constant [9 x i8] c"g.return\00"
@_ZSt4cout = external global %"struct.std::basic_ostream<char,std::char_traits<char> >"
@_ZStL8__ioinit = internal global %"struct.std::ios_base::Init" zeroinitializer
@__dso_handle = external global i8*
@llvm.global_ctors = appending global [1 x %0] [%0 { i32 65535, void ()* @_GLOBAL__I_recordBasicBlock }]

@_ZL20__gthrw_pthread_oncePiPFvvE = alias weak i32 (i32*, void ()*)* @pthread_once
@_ZL27__gthrw_pthread_getspecificj = alias weak i8* (i32)* @pthread_getspecific
@_ZL27__gthrw_pthread_setspecificjPKv = alias weak i32 (i32, i8*)* @pthread_setspecific
@_ZL22__gthrw_pthread_createPmPK14pthread_attr_tPFPvS3_ES3_ = alias weak i32 (i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*)* @pthread_create
@_ZL22__gthrw_pthread_cancelm = alias weak i32 (i32)* @pthread_cancel
@_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_lock
@_ZL29__gthrw_pthread_mutex_trylockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_trylock
@_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_unlock
@_ZL26__gthrw_pthread_mutex_initP15pthread_mutex_tPK19pthread_mutexattr_t = alias weak i32 (%union.pthread_mutex_t*, %union..0._10*)* @pthread_mutex_init
@_ZL26__gthrw_pthread_key_createPjPFvPvE = alias weak i32 (i32*, void (i8*)*)* @pthread_key_create
@_ZL26__gthrw_pthread_key_deletej = alias weak i32 (i32)* @pthread_key_delete
@_ZL30__gthrw_pthread_mutexattr_initP19pthread_mutexattr_t = alias weak i32 (%union..0._10*)* @pthread_mutexattr_init
@_ZL33__gthrw_pthread_mutexattr_settypeP19pthread_mutexattr_ti = alias weak i32 (%union..0._10*, i32)* @pthread_mutexattr_settype
@_ZL33__gthrw_pthread_mutexattr_destroyP19pthread_mutexattr_t = alias weak i32 (%union..0._10*)* @pthread_mutexattr_destroy

define i32 @main(i32 %argc, i8** nocapture %argv) nounwind {
entry:
  %0 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([11 x i8]* @0, i32 0, i32 0)) nounwind
  %1 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %0, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  call void @llvm.dbg.declare(metadata !15, metadata !16), !dbg !17
  call void @llvm.dbg.declare(metadata !15, metadata !18), !dbg !17
  call void @llvm.dbg.declare(metadata !15, metadata !19), !dbg !21
  call void @llvm.dbg.declare(metadata !15, metadata !22), !dbg !23
  %2 = getelementptr inbounds i8** %argv, i32 1, !dbg !21
  %3 = load i8** %2, align 1, !dbg !21
  %4 = call i32 @atoi(i8* %3) nounwind readonly, !dbg !21
  %5 = getelementptr inbounds i8** %argv, i32 2, !dbg !23
  %6 = load i8** %5, align 1, !dbg !23
  %7 = call i32 @atoi(i8* %6) nounwind readonly, !dbg !23
  %8 = icmp sgt i32 %4, %7, !dbg !24
  br i1 %8, label %bb, label %bb1, !dbg !24

bb:                                               ; preds = %entry
  %9 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([8 x i8]* @1, i32 0, i32 0)) nounwind
  %10 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %9, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %11 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([8 x i8]* @4, i32 0, i32 0)) nounwind
  %12 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %11, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  call void @llvm.dbg.declare(metadata !15, metadata !25) nounwind, !dbg !26
  %13 = icmp sgt i32 %4, 0, !dbg !28
  br i1 %13, label %bb.i, label %f.exit, !dbg !28

bb.i:                                             ; preds = %bb
  %14 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([5 x i8]* @5, i32 0, i32 0)) nounwind
  %15 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %14, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  call void @exit(i32 0) noreturn nounwind, !dbg !30
  unreachable, !dbg !30

f.exit:                                           ; preds = %bb
  %16 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @6, i32 0, i32 0)) nounwind
  %17 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %16, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %18 = sub nsw i32 0, %4, !dbg !31
  %19 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([9 x i8]* @7, i32 0, i32 0)) nounwind
  %20 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %19, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  br label %bb1, !dbg !27

bb1:                                              ; preds = %f.exit, %entry
  %x.0 = phi i32 [ %18, %f.exit ], [ %4, %entry ]
  %21 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([9 x i8]* @2, i32 0, i32 0)) nounwind
  %22 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %21, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %23 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([8 x i8]* @8, i32 0, i32 0)) nounwind
  %24 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %23, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  call void @llvm.dbg.declare(metadata !15, metadata !32) nounwind, !dbg !33
  call void @llvm.dbg.declare(metadata !15, metadata !35) nounwind, !dbg !33
  %25 = icmp eq i32 %x.0, 0, !dbg !36
  br i1 %25, label %bb.i2, label %bb6.i, !dbg !36

bb.i2:                                            ; preds = %bb1
  %26 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([5 x i8]* @9, i32 0, i32 0)) nounwind
  %27 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %26, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %28 = icmp eq i32 %7, 0, !dbg !38
  br i1 %28, label %bb1.i, label %bb2.i, !dbg !38

bb1.i:                                            ; preds = %bb.i2
  %29 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @10, i32 0, i32 0)) nounwind
  %30 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %29, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  call void @exit(i32 0) noreturn nounwind, !dbg !39
  unreachable, !dbg !39

bb2.i:                                            ; preds = %bb.i2
  %31 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @11, i32 0, i32 0)) nounwind
  %32 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %31, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  br label %bb4.i, !dbg !39

bb3.i:                                            ; preds = %bb4.i
  %33 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @12, i32 0, i32 0)) nounwind
  %34 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %33, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %35 = add nsw i32 %a_addr.i1.0, 1, !dbg !40
  br label %bb4.i, !dbg !40

bb4.i:                                            ; preds = %bb3.i, %bb2.i
  %a_addr.i1.0 = phi i32 [ %x.0, %bb2.i ], [ %35, %bb3.i ]
  %36 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @13, i32 0, i32 0)) nounwind
  %37 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %36, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %38 = icmp slt i32 %a_addr.i1.0, 10, !dbg !41
  br i1 %38, label %bb3.i, label %bb5.i, !dbg !41

bb5.i:                                            ; preds = %bb4.i
  %39 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @14, i32 0, i32 0)) nounwind
  %40 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %39, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  br label %return, !dbg !41

bb6.i:                                            ; preds = %bb1
  %41 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @15, i32 0, i32 0)) nounwind
  %42 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %41, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %43 = sdiv i32 %x.0, %7, !dbg !42
  %44 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0), i32 %43) nounwind, !dbg !42
  br label %return, !dbg !42

return:                                           ; preds = %bb6.i, %bb5.i
  %45 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @16, i32 0, i32 0)) nounwind
  %46 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %45, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %47 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([9 x i8]* @17, i32 0, i32 0)) nounwind
  %48 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %47, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %49 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4cout, i8* getelementptr inbounds ([12 x i8]* @3, i32 0, i32 0)) nounwind
  %50 = call %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"* %49, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  ret i32 0, !dbg !43
}

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

declare i32 @atoi(i8*) nounwind readonly

declare void @exit(i32) noreturn nounwind

declare i32 @printf(i8* noalias, ...) nounwind

define internal void @_GLOBAL__I_recordBasicBlock() {
return:
  call void @_ZNSt8ios_base4InitC1Ev(%"struct.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @__cxa_atexit(void (i8*)* @__tcf_0, i8* null, i8* bitcast (i8** @__dso_handle to i8*)) nounwind
  ret void
}

declare %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::basic_ostream<char,std::char_traits<char> >"*, i8*)

declare %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZNSolsEPFRSoS_E(%"struct.std::basic_ostream<char,std::char_traits<char> >"*, %"struct.std::basic_ostream<char,std::char_traits<char> >"* (%"struct.std::basic_ostream<char,std::char_traits<char> >"*)*)

declare %"struct.std::basic_ostream<char,std::char_traits<char> >"* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%"struct.std::basic_ostream<char,std::char_traits<char> >"*)

declare void @_ZNSt8ios_base4InitC1Ev(%"struct.std::ios_base::Init"*)

declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) nounwind

define internal void @__tcf_0(i8* nocapture %unnamed_arg) {
return:
  call void @_ZNSt8ios_base4InitD1Ev(%"struct.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

declare void @_ZNSt8ios_base4InitD1Ev(%"struct.std::ios_base::Init"*)

declare extern_weak i32 @pthread_once(i32*, void ()*)

declare extern_weak i8* @pthread_getspecific(i32)

declare extern_weak i32 @pthread_setspecific(i32, i8*)

declare extern_weak i32 @pthread_create(i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*)

declare extern_weak i32 @pthread_cancel(i32)

declare extern_weak i32 @pthread_mutex_lock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_trylock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_unlock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_init(%union.pthread_mutex_t*, %union..0._10*)

declare extern_weak i32 @pthread_key_create(i32*, void (i8*)*)

declare extern_weak i32 @pthread_key_delete(i32)

declare extern_weak i32 @pthread_mutexattr_init(%union..0._10*)

declare extern_weak i32 @pthread_mutexattr_settype(%union..0._10*, i32)

declare extern_weak i32 @pthread_mutexattr_destroy(%union..0._10*)

!llvm.dbg.sp = !{!0, !9, !12}

!0 = metadata !{i32 589870, i32 0, metadata !1, metadata !"main", metadata !"main", metadata !"main", metadata !1, i32 6, metadata !3, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, i8**)* @main} ; [ DW_TAG_subprogram ]
!1 = metadata !{i32 589865, metadata !"1.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !2} ; [ DW_TAG_file_type ]
!2 = metadata !{i32 589841, i32 0, i32 1, metadata !"1.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !"4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2.9)", i1 true, i1 false, metadata !"", i32 0} ; [ DW_TAG_compile_unit ]
!3 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !4, i32 0, null} ; [ DW_TAG_subroutine_type ]
!4 = metadata !{metadata !5, metadata !5, metadata !6}
!5 = metadata !{i32 589860, metadata !1, metadata !"int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!6 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !7} ; [ DW_TAG_pointer_type ]
!7 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !8} ; [ DW_TAG_pointer_type ]
!8 = metadata !{i32 589860, metadata !1, metadata !"char", metadata !1, i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ]
!9 = metadata !{i32 589870, i32 0, metadata !1, metadata !"f", metadata !"f", metadata !"f", metadata !1, i32 18, metadata !10, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null} ; [ DW_TAG_subprogram ]
!10 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !11, i32 0, null} ; [ DW_TAG_subroutine_type ]
!11 = metadata !{metadata !5, metadata !5}
!12 = metadata !{i32 589870, i32 0, metadata !1, metadata !"g", metadata !"g", metadata !"g", metadata !1, i32 25, metadata !13, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null} ; [ DW_TAG_subprogram ]
!13 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !14, i32 0, null} ; [ DW_TAG_subroutine_type ]
!14 = metadata !{null, metadata !5, metadata !5}
!15 = metadata !{null}
!16 = metadata !{i32 590081, metadata !0, metadata !"argc", metadata !1, i32 6, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!17 = metadata !{i32 6, i32 0, metadata !0, null}
!18 = metadata !{i32 590081, metadata !0, metadata !"argv", metadata !1, i32 6, metadata !6, i32 0} ; [ DW_TAG_arg_variable ]
!19 = metadata !{i32 590080, metadata !20, metadata !"x", metadata !1, i32 7, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!20 = metadata !{i32 589835, metadata !0, i32 6, i32 0, metadata !1, i32 0} ; [ DW_TAG_lexical_block ]
!21 = metadata !{i32 7, i32 0, metadata !20, null}
!22 = metadata !{i32 590080, metadata !20, metadata !"y", metadata !1, i32 8, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!23 = metadata !{i32 8, i32 0, metadata !20, null}
!24 = metadata !{i32 9, i32 0, metadata !20, null}
!25 = metadata !{i32 590081, metadata !9, metadata !"a", metadata !1, i32 18, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!26 = metadata !{i32 18, i32 0, metadata !9, metadata !27}
!27 = metadata !{i32 10, i32 0, metadata !20, null}
!28 = metadata !{i32 19, i32 0, metadata !29, metadata !27}
!29 = metadata !{i32 589835, metadata !9, i32 18, i32 0, metadata !1, i32 1} ; [ DW_TAG_lexical_block ]
!30 = metadata !{i32 20, i32 0, metadata !29, metadata !27}
!31 = metadata !{i32 22, i32 0, metadata !29, metadata !27}
!32 = metadata !{i32 590081, metadata !12, metadata !"a", metadata !1, i32 25, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!33 = metadata !{i32 25, i32 0, metadata !12, metadata !34}
!34 = metadata !{i32 14, i32 0, metadata !20, null}
!35 = metadata !{i32 590081, metadata !12, metadata !"b", metadata !1, i32 25, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!36 = metadata !{i32 26, i32 0, metadata !37, metadata !34}
!37 = metadata !{i32 589835, metadata !12, i32 25, i32 0, metadata !1, i32 2} ; [ DW_TAG_lexical_block ]
!38 = metadata !{i32 27, i32 0, metadata !37, metadata !34}
!39 = metadata !{i32 28, i32 0, metadata !37, metadata !34}
!40 = metadata !{i32 31, i32 0, metadata !37, metadata !34}
!41 = metadata !{i32 30, i32 0, metadata !37, metadata !34}
!42 = metadata !{i32 34, i32 0, metadata !37, metadata !34}
!43 = metadata !{i32 15, i32 0, metadata !20, null}
