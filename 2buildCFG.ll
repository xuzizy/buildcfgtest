; ModuleID = '2buildCFG.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

@.str = private unnamed_addr constant [14 x i8] c"x= %d,y = %d\0A\00", align 1
@.str1 = private unnamed_addr constant [10 x i8] c"%d! = %d\0A\00", align 1
@0 = internal constant [54 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/2.c: 17\00"
@1 = internal constant [11 x i8] c"main.entry\00"
@2 = internal constant [54 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/2.c: 17\00"
@3 = internal constant [12 x i8] c"main.return\00"
@4 = internal constant [54 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/2.c: 25\00"
@5 = internal constant [11 x i8] c"fact.entry\00"
@6 = internal constant [54 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/2.c: 25\00"
@7 = internal constant [8 x i8] c"fact.bb\00"
@8 = internal constant [54 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/2.c: 26\00"
@9 = internal constant [9 x i8] c"fact.bb1\00"
@10 = internal constant [54 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/2.c: 25\00"
@11 = internal constant [9 x i8] c"fact.bb2\00"
@12 = internal constant [54 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/2.c: 25\00"
@13 = internal constant [12 x i8] c"fact.return\00"

define i32 @main(i32 %argc, i8** %argv) nounwind {
entry:
  %0 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([11 x i8]* @1, i32 0, i32 0), i8* getelementptr inbounds ([54 x i8]* @0, i32 0, i32 0))
  %argc_addr = alloca i32, align 4
  %argv_addr = alloca i8**, align 4
  %retval = alloca i32
  %1 = alloca i32
  %x = alloca i32
  %y = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %argc_addr}, metadata !12), !dbg !13
  store i32 %argc, i32* %argc_addr
  call void @llvm.dbg.declare(metadata !{i8*** %argv_addr}, metadata !14), !dbg !13
  store i8** %argv, i8*** %argv_addr
  call void @llvm.dbg.declare(metadata !{i32* %x}, metadata !15), !dbg !17
  call void @llvm.dbg.declare(metadata !{i32* %y}, metadata !18), !dbg !19
  %2 = load i8*** %argv_addr, align 4, !dbg !17
  %3 = getelementptr inbounds i8** %2, i32 1, !dbg !17
  %4 = load i8** %3, align 1, !dbg !17
  %5 = call i32 @atoi(i8* %4) nounwind readonly, !dbg !17
  store i32 %5, i32* %x, align 4, !dbg !17
  %6 = load i8*** %argv_addr, align 4, !dbg !19
  %7 = getelementptr inbounds i8** %6, i32 2, !dbg !19
  %8 = load i8** %7, align 1, !dbg !19
  %9 = call i32 @atoi(i8* %8) nounwind readonly, !dbg !19
  store i32 %9, i32* %y, align 4, !dbg !19
  %10 = load i32* %x, align 4, !dbg !20
  %11 = load i32* %y, align 4, !dbg !20
  %12 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([14 x i8]* @.str, i32 0, i32 0), i32 %10, i32 %11) nounwind, !dbg !20
  %13 = load i32* %x, align 4, !dbg !21
  %14 = call i32 @fact(i32 %13) nounwind, !dbg !21
  store i32 %14, i32* %y, align 4, !dbg !21
  %15 = load i32* %x, align 4, !dbg !22
  %16 = load i32* %y, align 4, !dbg !22
  %17 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([10 x i8]* @.str1, i32 0, i32 0), i32 %15, i32 %16) nounwind, !dbg !22
  store i32 0, i32* %1, align 4, !dbg !23
  %18 = load i32* %1, align 4, !dbg !23
  store i32 %18, i32* %retval, align 4, !dbg !23
  br label %return, !dbg !23

return:                                           ; preds = %entry
  %19 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([12 x i8]* @3, i32 0, i32 0), i8* getelementptr inbounds ([54 x i8]* @2, i32 0, i32 0))
  %retval1 = load i32* %retval, !dbg !23
  ret i32 %retval1, !dbg !23
}

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

declare i32 @atoi(i8*) nounwind readonly

declare i32 @printf(i8* noalias, ...) nounwind

define i32 @fact(i32 %a) nounwind {
entry:
  %0 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([11 x i8]* @5, i32 0, i32 0), i8* getelementptr inbounds ([54 x i8]* @4, i32 0, i32 0))
  %a_addr = alloca i32, align 4
  %retval = alloca i32
  %1 = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %a_addr}, metadata !24), !dbg !25
  store i32 %a, i32* %a_addr
  %2 = load i32* %a_addr, align 4, !dbg !26
  %3 = icmp sle i32 %2, 1, !dbg !26
  br i1 %3, label %bb, label %bb1, !dbg !26

bb:                                               ; preds = %entry
  %4 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([8 x i8]* @7, i32 0, i32 0), i8* getelementptr inbounds ([54 x i8]* @6, i32 0, i32 0))
  store i32 1, i32* %1, align 4, !dbg !26
  br label %bb2, !dbg !26

bb1:                                              ; preds = %entry
  %5 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([9 x i8]* @9, i32 0, i32 0), i8* getelementptr inbounds ([54 x i8]* @8, i32 0, i32 0))
  %6 = load i32* %a_addr, align 4, !dbg !28
  %7 = sub nsw i32 %6, 1, !dbg !28
  %8 = call i32 @fact(i32 %7) nounwind, !dbg !28
  %9 = load i32* %a_addr, align 4, !dbg !28
  %10 = mul nsw i32 %8, %9, !dbg !28
  store i32 %10, i32* %1, align 4, !dbg !28
  br label %bb2, !dbg !28

bb2:                                              ; preds = %bb1, %bb
  %11 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([9 x i8]* @11, i32 0, i32 0), i8* getelementptr inbounds ([54 x i8]* @10, i32 0, i32 0))
  %12 = load i32* %1, align 4, !dbg !26
  store i32 %12, i32* %retval, align 4, !dbg !26
  br label %return, !dbg !26

return:                                           ; preds = %bb2
  %13 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([12 x i8]* @13, i32 0, i32 0), i8* getelementptr inbounds ([54 x i8]* @12, i32 0, i32 0))
  %retval3 = load i32* %retval, !dbg !26
  ret i32 %retval3, !dbg !26
}

declare i32 @recordBasicBlock(i8*, i8*)

!llvm.dbg.sp = !{!0, !9}

!0 = metadata !{i32 589870, i32 0, metadata !1, metadata !"main", metadata !"main", metadata !"main", metadata !1, i32 6, metadata !3, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, i8**)* @main} ; [ DW_TAG_subprogram ]
!1 = metadata !{i32 589865, metadata !"2.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !2} ; [ DW_TAG_file_type ]
!2 = metadata !{i32 589841, i32 0, i32 1, metadata !"2.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !"4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2.9)", i1 true, i1 false, metadata !"", i32 0} ; [ DW_TAG_compile_unit ]
!3 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !4, i32 0, null} ; [ DW_TAG_subroutine_type ]
!4 = metadata !{metadata !5, metadata !5, metadata !6}
!5 = metadata !{i32 589860, metadata !1, metadata !"int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!6 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !7} ; [ DW_TAG_pointer_type ]
!7 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !8} ; [ DW_TAG_pointer_type ]
!8 = metadata !{i32 589860, metadata !1, metadata !"char", metadata !1, i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ]
!9 = metadata !{i32 589870, i32 0, metadata !1, metadata !"fact", metadata !"fact", metadata !"fact", metadata !1, i32 24, metadata !10, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32)* @fact} ; [ DW_TAG_subprogram ]
!10 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !11, i32 0, null} ; [ DW_TAG_subroutine_type ]
!11 = metadata !{metadata !5, metadata !5}
!12 = metadata !{i32 590081, metadata !0, metadata !"argc", metadata !1, i32 6, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!13 = metadata !{i32 6, i32 0, metadata !0, null}
!14 = metadata !{i32 590081, metadata !0, metadata !"argv", metadata !1, i32 6, metadata !6, i32 0} ; [ DW_TAG_arg_variable ]
!15 = metadata !{i32 590080, metadata !16, metadata !"x", metadata !1, i32 11, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!16 = metadata !{i32 589835, metadata !0, i32 6, i32 0, metadata !1, i32 0} ; [ DW_TAG_lexical_block ]
!17 = metadata !{i32 11, i32 0, metadata !16, null}
!18 = metadata !{i32 590080, metadata !16, metadata !"y", metadata !1, i32 12, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!19 = metadata !{i32 12, i32 0, metadata !16, null}
!20 = metadata !{i32 13, i32 0, metadata !16, null}
!21 = metadata !{i32 15, i32 0, metadata !16, null}
!22 = metadata !{i32 16, i32 0, metadata !16, null}
!23 = metadata !{i32 17, i32 0, metadata !16, null}
!24 = metadata !{i32 590081, metadata !9, metadata !"a", metadata !1, i32 24, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!25 = metadata !{i32 24, i32 0, metadata !9, null}
!26 = metadata !{i32 25, i32 0, metadata !27, null}
!27 = metadata !{i32 589835, metadata !9, i32 24, i32 0, metadata !1, i32 1} ; [ DW_TAG_lexical_block ]
!28 = metadata !{i32 26, i32 0, metadata !27, null}
