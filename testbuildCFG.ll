; ModuleID = 'testbuildCFG.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

@0 = internal constant [24 x i8] c"can't find debug info: \00"
@1 = internal constant [8 x i8] c"g.entry\00"
@2 = internal constant [56 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/test.c: 7\00"
@3 = internal constant [9 x i8] c"g.return\00"
@4 = internal constant [24 x i8] c"can't find debug info: \00"
@5 = internal constant [8 x i8] c"f.entry\00"
@6 = internal constant [57 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/test.c: 12\00"
@7 = internal constant [5 x i8] c"f.bb\00"
@8 = internal constant [57 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/test.c: 14\00"
@9 = internal constant [6 x i8] c"f.bb1\00"
@10 = internal constant [57 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/test.c: 14\00"
@11 = internal constant [9 x i8] c"f.return\00"
@12 = internal constant [24 x i8] c"can't find debug info: \00"
@13 = internal constant [8 x i8] c"h.entry\00"
@14 = internal constant [57 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/test.c: 18\00"
@15 = internal constant [9 x i8] c"h.return\00"
@16 = internal constant [24 x i8] c"can't find debug info: \00"
@17 = internal constant [11 x i8] c"main.entry\00"
@18 = internal constant [57 x i8] c"/home/zhouyan/work/test/test_length_n_subpath/test.c: 56\00"
@19 = internal constant [12 x i8] c"main.return\00"

define void @g(i32 %n, i32 %m) nounwind {
entry:
  %0 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([8 x i8]* @1, i32 0, i32 0), i8* getelementptr inbounds ([24 x i8]* @0, i32 0, i32 0))
  %n_addr = alloca i32, align 4
  %m_addr = alloca i32, align 4
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %n_addr}, metadata !11), !dbg !12
  store i32 %n, i32* %n_addr
  call void @llvm.dbg.declare(metadata !{i32* %m_addr}, metadata !13), !dbg !12
  store i32 %m, i32* %m_addr
  %1 = load i32* %n_addr, align 4, !dbg !14
  %2 = load i32* %m_addr, align 4, !dbg !14
  call void @f(i32 %1, i32 %2) nounwind, !dbg !14
  br label %return, !dbg !16

return:                                           ; preds = %entry
  %3 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([9 x i8]* @3, i32 0, i32 0), i8* getelementptr inbounds ([56 x i8]* @2, i32 0, i32 0))
  ret void, !dbg !16
}

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

define void @f(i32 %n, i32 %m) nounwind {
entry:
  %0 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([8 x i8]* @5, i32 0, i32 0), i8* getelementptr inbounds ([24 x i8]* @4, i32 0, i32 0))
  %n_addr = alloca i32, align 4
  %m_addr = alloca i32, align 4
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %n_addr}, metadata !17), !dbg !18
  store i32 %n, i32* %n_addr
  call void @llvm.dbg.declare(metadata !{i32* %m_addr}, metadata !19), !dbg !18
  store i32 %m, i32* %m_addr
  %1 = load i32* %n_addr, align 4, !dbg !20
  %2 = load i32* %m_addr, align 4, !dbg !20
  %3 = icmp slt i32 %1, %2, !dbg !20
  br i1 %3, label %bb, label %bb1, !dbg !20

bb:                                               ; preds = %entry
  %4 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([5 x i8]* @7, i32 0, i32 0), i8* getelementptr inbounds ([57 x i8]* @6, i32 0, i32 0))
  %5 = load i32* %n_addr, align 4, !dbg !22
  %6 = add nsw i32 %5, 1, !dbg !22
  %7 = load i32* %m_addr, align 4, !dbg !22
  call void @g(i32 %6, i32 %7) nounwind, !dbg !22
  br label %bb1, !dbg !22

bb1:                                              ; preds = %bb, %entry
  %8 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([6 x i8]* @9, i32 0, i32 0), i8* getelementptr inbounds ([57 x i8]* @8, i32 0, i32 0))
  br label %return, !dbg !23

return:                                           ; preds = %bb1
  %9 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([9 x i8]* @11, i32 0, i32 0), i8* getelementptr inbounds ([57 x i8]* @10, i32 0, i32 0))
  ret void, !dbg !23
}

define i32 @h() nounwind {
entry:
  %0 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([8 x i8]* @13, i32 0, i32 0), i8* getelementptr inbounds ([24 x i8]* @12, i32 0, i32 0))
  %retval = alloca i32
  %1 = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  store i32 0, i32* %1, align 4, !dbg !24
  %2 = load i32* %1, align 4, !dbg !24
  store i32 %2, i32* %retval, align 4, !dbg !24
  br label %return, !dbg !24

return:                                           ; preds = %entry
  %3 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([9 x i8]* @15, i32 0, i32 0), i8* getelementptr inbounds ([57 x i8]* @14, i32 0, i32 0))
  %retval1 = load i32* %retval, !dbg !24
  ret i32 %retval1, !dbg !24
}

define i32 @main() nounwind {
entry:
  %0 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([11 x i8]* @17, i32 0, i32 0), i8* getelementptr inbounds ([24 x i8]* @16, i32 0, i32 0))
  %retval = alloca i32
  %1 = alloca i32
  %n = alloca i32
  %m = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %n}, metadata !26), !dbg !28
  call void @llvm.dbg.declare(metadata !{i32* %m}, metadata !29), !dbg !28
  store i32 1, i32* %n, align 4, !dbg !28
  store i32 5, i32* %m, align 4, !dbg !28
  %2 = call i32 @h() nounwind, !dbg !30
  %3 = call i32 @h() nounwind, !dbg !31
  store i32 0, i32* %1, align 4, !dbg !32
  %4 = load i32* %1, align 4, !dbg !32
  store i32 %4, i32* %retval, align 4, !dbg !32
  br label %return, !dbg !32

return:                                           ; preds = %entry
  %5 = call i32 @recordBasicBlock(i8* getelementptr inbounds ([12 x i8]* @19, i32 0, i32 0), i8* getelementptr inbounds ([57 x i8]* @18, i32 0, i32 0))
  %retval1 = load i32* %retval, !dbg !32
  %6 = call i32 @coverageOutput()
  ret i32 %retval1, !dbg !32
}

declare i32 @recordBasicBlock(i8*, i8*)

declare i32 @coverageOutput()

!llvm.dbg.sp = !{!0, !6, !7, !10}

!0 = metadata !{i32 589870, i32 0, metadata !1, metadata !"g", metadata !"g", metadata !"g", metadata !1, i32 5, metadata !3, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32)* @g} ; [ DW_TAG_subprogram ]
!1 = metadata !{i32 589865, metadata !"test.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !2} ; [ DW_TAG_file_type ]
!2 = metadata !{i32 589841, i32 0, i32 1, metadata !"test.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !"4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2.9)", i1 true, i1 false, metadata !"", i32 0} ; [ DW_TAG_compile_unit ]
!3 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !4, i32 0, null} ; [ DW_TAG_subroutine_type ]
!4 = metadata !{null, metadata !5, metadata !5}
!5 = metadata !{i32 589860, metadata !1, metadata !"int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!6 = metadata !{i32 589870, i32 0, metadata !1, metadata !"f", metadata !"f", metadata !"f", metadata !1, i32 9, metadata !3, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32)* @f} ; [ DW_TAG_subprogram ]
!7 = metadata !{i32 589870, i32 0, metadata !1, metadata !"h", metadata !"h", metadata !"h", metadata !1, i32 17, metadata !8, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @h} ; [ DW_TAG_subprogram ]
!8 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !9, i32 0, null} ; [ DW_TAG_subroutine_type ]
!9 = metadata !{metadata !5}
!10 = metadata !{i32 589870, i32 0, metadata !1, metadata !"main", metadata !"main", metadata !"main", metadata !1, i32 22, metadata !8, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @main} ; [ DW_TAG_subprogram ]
!11 = metadata !{i32 590081, metadata !0, metadata !"n", metadata !1, i32 5, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!12 = metadata !{i32 5, i32 0, metadata !0, null}
!13 = metadata !{i32 590081, metadata !0, metadata !"m", metadata !1, i32 5, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!14 = metadata !{i32 6, i32 0, metadata !15, null}
!15 = metadata !{i32 589835, metadata !0, i32 5, i32 0, metadata !1, i32 0} ; [ DW_TAG_lexical_block ]
!16 = metadata !{i32 7, i32 0, metadata !15, null}
!17 = metadata !{i32 590081, metadata !6, metadata !"n", metadata !1, i32 9, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!18 = metadata !{i32 9, i32 0, metadata !6, null}
!19 = metadata !{i32 590081, metadata !6, metadata !"m", metadata !1, i32 9, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!20 = metadata !{i32 11, i32 0, metadata !21, null}
!21 = metadata !{i32 589835, metadata !6, i32 9, i32 0, metadata !1, i32 1} ; [ DW_TAG_lexical_block ]
!22 = metadata !{i32 12, i32 0, metadata !21, null}
!23 = metadata !{i32 14, i32 0, metadata !21, null}
!24 = metadata !{i32 18, i32 0, metadata !25, null}
!25 = metadata !{i32 589835, metadata !7, i32 17, i32 0, metadata !1, i32 2} ; [ DW_TAG_lexical_block ]
!26 = metadata !{i32 590080, metadata !27, metadata !"n", metadata !1, i32 24, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!27 = metadata !{i32 589835, metadata !10, i32 22, i32 0, metadata !1, i32 3} ; [ DW_TAG_lexical_block ]
!28 = metadata !{i32 24, i32 0, metadata !27, null}
!29 = metadata !{i32 590080, metadata !27, metadata !"m", metadata !1, i32 24, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!30 = metadata !{i32 43, i32 0, metadata !27, null}
!31 = metadata !{i32 44, i32 0, metadata !27, null}
!32 = metadata !{i32 56, i32 0, metadata !27, null}
