; ModuleID = '1.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i32, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i32, i32, [40 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@stderr = external unnamed_addr global %struct._IO_FILE*
@.str = private unnamed_addr constant [28 x i8] c"usage: ./program int1 int2\0A\00", align 1
@.str1 = private unnamed_addr constant [7 x i8] c"x = %d\00", align 1
@.str2 = private unnamed_addr constant [10 x i8] c"a/b = %d\0A\00", align 1

define i32 @main(i32 %argc, i8** %argv) nounwind {
entry:
  %argc_addr = alloca i32, align 4
  %argv_addr = alloca i8**, align 4
  %retval = alloca i32
  %0 = alloca i32
  %x = alloca i32
  %y = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %argc_addr}, metadata !15), !dbg !16
  store i32 %argc, i32* %argc_addr
  call void @llvm.dbg.declare(metadata !{i8*** %argv_addr}, metadata !17), !dbg !16
  store i8** %argv, i8*** %argv_addr
  call void @llvm.dbg.declare(metadata !{i32* %x}, metadata !18), !dbg !20
  call void @llvm.dbg.declare(metadata !{i32* %y}, metadata !21), !dbg !22
  %1 = load i32* %argc_addr, align 4, !dbg !23
  %2 = icmp ne i32 %1, 3, !dbg !23
  br i1 %2, label %bb, label %bb1, !dbg !23

bb:                                               ; preds = %entry
  %3 = load %struct._IO_FILE** @stderr, align 4, !dbg !24
  %4 = bitcast %struct._IO_FILE* %3 to i8*, !dbg !24
  %5 = call i32 @fwrite(i8* getelementptr inbounds ([28 x i8]* @.str, i32 0, i32 0), i32 1, i32 27, i8* %4) nounwind, !dbg !24
  call void @exit(i32 -1) noreturn nounwind, !dbg !25
  unreachable, !dbg !25

bb1:                                              ; preds = %entry
  %6 = load i8*** %argv_addr, align 4, !dbg !20
  %7 = getelementptr inbounds i8** %6, i32 1, !dbg !20
  %8 = load i8** %7, align 1, !dbg !20
  %9 = call i32 @atoi(i8* %8) nounwind readonly, !dbg !20
  store i32 %9, i32* %x, align 4, !dbg !20
  %10 = load i8*** %argv_addr, align 4, !dbg !22
  %11 = getelementptr inbounds i8** %10, i32 2, !dbg !22
  %12 = load i8** %11, align 1, !dbg !22
  %13 = call i32 @atoi(i8* %12) nounwind readonly, !dbg !22
  store i32 %13, i32* %y, align 4, !dbg !22
  %14 = load i32* %x, align 4, !dbg !26
  %15 = load i32* %y, align 4, !dbg !26
  %16 = icmp sgt i32 %14, %15, !dbg !26
  br i1 %16, label %bb2, label %bb3, !dbg !26

bb2:                                              ; preds = %bb1
  %17 = load i32* %x, align 4, !dbg !27
  %18 = call i32 @f(i32 %17) nounwind, !dbg !27
  store i32 %18, i32* %x, align 4, !dbg !27
  br label %bb4, !dbg !27

bb3:                                              ; preds = %bb1
  %19 = load i32* %y, align 4, !dbg !28
  %20 = add nsw i32 %19, 1, !dbg !28
  store i32 %20, i32* %y, align 4, !dbg !28
  br label %bb4, !dbg !28

bb4:                                              ; preds = %bb3, %bb2
  %21 = load i32* %x, align 4, !dbg !29
  %22 = load i32* %y, align 4, !dbg !29
  call void @g(i32 %21, i32 %22) nounwind, !dbg !29
  %23 = load i32* %x, align 4, !dbg !30
  %24 = load i32* %y, align 4, !dbg !30
  call void @g(i32 %23, i32 %24) nounwind, !dbg !30
  %25 = load i32* %x, align 4, !dbg !31
  %26 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([7 x i8]* @.str1, i32 0, i32 0), i32 %25) nounwind, !dbg !31
  %27 = load i32* %x, align 4, !dbg !32
  switch i32 %27, label %bb6 [
    i32 0, label %bb5
  ], !dbg !32

bb5:                                              ; preds = %bb4
  %28 = load i32* %x, align 4, !dbg !33
  %29 = load i32* %y, align 4, !dbg !33
  call void @g(i32 %28, i32 %29) nounwind, !dbg !33
  store i32 0, i32* %0, align 4, !dbg !34
  br label %bb7, !dbg !34

bb6:                                              ; preds = %bb4
  store i32 -1, i32* %0, align 4, !dbg !35
  br label %bb7, !dbg !35

bb7:                                              ; preds = %bb6, %bb5
  %30 = load i32* %0, align 4, !dbg !35
  store i32 %30, i32* %retval, align 4, !dbg !35
  br label %return, !dbg !35

return:                                           ; preds = %bb7
  %retval8 = load i32* %retval, !dbg !35
  ret i32 %retval8, !dbg !35
}

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

declare i32 @fwrite(i8*, i32, i32, i8*)

declare void @exit(i32) noreturn nounwind

declare i32 @atoi(i8*) nounwind readonly

declare i32 @printf(i8* noalias, ...) nounwind

define i32 @f(i32 %a) nounwind {
entry:
  %a_addr = alloca i32, align 4
  %retval = alloca i32
  %0 = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %a_addr}, metadata !36), !dbg !37
  store i32 %a, i32* %a_addr
  %1 = load i32* %a_addr, align 4, !dbg !38
  %2 = icmp sgt i32 %1, 0, !dbg !38
  br i1 %2, label %bb, label %bb1, !dbg !38

bb:                                               ; preds = %entry
  call void @exit(i32 0) noreturn nounwind, !dbg !40
  unreachable, !dbg !40

bb1:                                              ; preds = %entry
  %3 = load i32* %a_addr, align 4, !dbg !41
  %4 = sub nsw i32 0, %3, !dbg !41
  store i32 %4, i32* %0, align 4, !dbg !41
  %5 = load i32* %0, align 4, !dbg !41
  store i32 %5, i32* %retval, align 4, !dbg !41
  br label %return, !dbg !41

return:                                           ; preds = %bb1
  %retval2 = load i32* %retval, !dbg !41
  ret i32 %retval2, !dbg !41
}

define void @g(i32 %a, i32 %b) nounwind {
entry:
  %a_addr = alloca i32, align 4
  %b_addr = alloca i32, align 4
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %a_addr}, metadata !42), !dbg !43
  store i32 %a, i32* %a_addr
  call void @llvm.dbg.declare(metadata !{i32* %b_addr}, metadata !44), !dbg !43
  store i32 %b, i32* %b_addr
  %0 = load i32* %a_addr, align 4, !dbg !45
  %1 = icmp eq i32 %0, 0, !dbg !45
  br i1 %1, label %bb, label %bb6, !dbg !45

bb:                                               ; preds = %entry
  %2 = load i32* %b_addr, align 4, !dbg !47
  %3 = icmp eq i32 %2, 0, !dbg !47
  br i1 %3, label %bb1, label %bb2, !dbg !47

bb1:                                              ; preds = %bb
  call void @exit(i32 0) noreturn nounwind, !dbg !48
  unreachable, !dbg !48

bb2:                                              ; preds = %bb
  br label %bb4, !dbg !48

bb3:                                              ; preds = %bb4
  %4 = load i32* %a_addr, align 4, !dbg !49
  %5 = add nsw i32 %4, 1, !dbg !49
  store i32 %5, i32* %a_addr, align 4, !dbg !49
  br label %bb4, !dbg !49

bb4:                                              ; preds = %bb3, %bb2
  %6 = load i32* %a_addr, align 4, !dbg !50
  %7 = icmp sle i32 %6, 9, !dbg !50
  br i1 %7, label %bb3, label %bb5, !dbg !50

bb5:                                              ; preds = %bb4
  br label %bb7, !dbg !50

bb6:                                              ; preds = %entry
  %8 = load i32* %a_addr, align 4, !dbg !51
  %9 = load i32* %b_addr, align 4, !dbg !51
  %10 = sdiv i32 %8, %9, !dbg !51
  %11 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([10 x i8]* @.str2, i32 0, i32 0), i32 %10) nounwind, !dbg !51
  br label %bb7, !dbg !51

bb7:                                              ; preds = %bb6, %bb5
  br label %return, !dbg !52

return:                                           ; preds = %bb7
  ret void, !dbg !52
}

!llvm.dbg.sp = !{!0, !9, !12}

!0 = metadata !{i32 589870, i32 0, metadata !1, metadata !"main", metadata !"main", metadata !"main", metadata !1, i32 6, metadata !3, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, i8**)* @main} ; [ DW_TAG_subprogram ]
!1 = metadata !{i32 589865, metadata !"1.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !2} ; [ DW_TAG_file_type ]
!2 = metadata !{i32 589841, i32 0, i32 1, metadata !"1.c", metadata !"/home/zhouyan/work/test/test_length_n_subpath/", metadata !"4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2.9)", i1 true, i1 false, metadata !"", i32 0} ; [ DW_TAG_compile_unit ]
!3 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !4, i32 0, null} ; [ DW_TAG_subroutine_type ]
!4 = metadata !{metadata !5, metadata !5, metadata !6}
!5 = metadata !{i32 589860, metadata !1, metadata !"int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!6 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !7} ; [ DW_TAG_pointer_type ]
!7 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !8} ; [ DW_TAG_pointer_type ]
!8 = metadata !{i32 589860, metadata !1, metadata !"char", metadata !1, i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ]
!9 = metadata !{i32 589870, i32 0, metadata !1, metadata !"f", metadata !"f", metadata !"f", metadata !1, i32 28, metadata !10, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32)* @f} ; [ DW_TAG_subprogram ]
!10 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !11, i32 0, null} ; [ DW_TAG_subroutine_type ]
!11 = metadata !{metadata !5, metadata !5}
!12 = metadata !{i32 589870, i32 0, metadata !1, metadata !"g", metadata !"g", metadata !"g", metadata !1, i32 35, metadata !13, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32)* @g} ; [ DW_TAG_subprogram ]
!13 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !14, i32 0, null} ; [ DW_TAG_subroutine_type ]
!14 = metadata !{null, metadata !5, metadata !5}
!15 = metadata !{i32 590081, metadata !0, metadata !"argc", metadata !1, i32 6, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!16 = metadata !{i32 6, i32 0, metadata !0, null}
!17 = metadata !{i32 590081, metadata !0, metadata !"argv", metadata !1, i32 6, metadata !6, i32 0} ; [ DW_TAG_arg_variable ]
!18 = metadata !{i32 590080, metadata !19, metadata !"x", metadata !1, i32 11, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!19 = metadata !{i32 589835, metadata !0, i32 6, i32 0, metadata !1, i32 0} ; [ DW_TAG_lexical_block ]
!20 = metadata !{i32 11, i32 0, metadata !19, null}
!21 = metadata !{i32 590080, metadata !19, metadata !"y", metadata !1, i32 12, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!22 = metadata !{i32 12, i32 0, metadata !19, null}
!23 = metadata !{i32 7, i32 0, metadata !19, null}
!24 = metadata !{i32 8, i32 0, metadata !19, null}
!25 = metadata !{i32 9, i32 0, metadata !19, null}
!26 = metadata !{i32 13, i32 0, metadata !19, null}
!27 = metadata !{i32 14, i32 0, metadata !19, null}
!28 = metadata !{i32 16, i32 0, metadata !19, null}
!29 = metadata !{i32 18, i32 0, metadata !19, null}
!30 = metadata !{i32 19, i32 0, metadata !19, null}
!31 = metadata !{i32 20, i32 0, metadata !19, null}
!32 = metadata !{i32 21, i32 0, metadata !19, null}
!33 = metadata !{i32 22, i32 0, metadata !19, null}
!34 = metadata !{i32 25, i32 0, metadata !19, null}
!35 = metadata !{i32 23, i32 0, metadata !19, null}
!36 = metadata !{i32 590081, metadata !9, metadata !"a", metadata !1, i32 28, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!37 = metadata !{i32 28, i32 0, metadata !9, null}
!38 = metadata !{i32 29, i32 0, metadata !39, null}
!39 = metadata !{i32 589835, metadata !9, i32 28, i32 0, metadata !1, i32 1} ; [ DW_TAG_lexical_block ]
!40 = metadata !{i32 30, i32 0, metadata !39, null}
!41 = metadata !{i32 32, i32 0, metadata !39, null}
!42 = metadata !{i32 590081, metadata !12, metadata !"a", metadata !1, i32 35, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!43 = metadata !{i32 35, i32 0, metadata !12, null}
!44 = metadata !{i32 590081, metadata !12, metadata !"b", metadata !1, i32 35, metadata !5, i32 0} ; [ DW_TAG_arg_variable ]
!45 = metadata !{i32 36, i32 0, metadata !46, null}
!46 = metadata !{i32 589835, metadata !12, i32 35, i32 0, metadata !1, i32 2} ; [ DW_TAG_lexical_block ]
!47 = metadata !{i32 37, i32 0, metadata !46, null}
!48 = metadata !{i32 38, i32 0, metadata !46, null}
!49 = metadata !{i32 41, i32 0, metadata !46, null}
!50 = metadata !{i32 40, i32 0, metadata !46, null}
!51 = metadata !{i32 44, i32 0, metadata !46, null}
!52 = metadata !{i32 46, i32 0, metadata !46, null}
